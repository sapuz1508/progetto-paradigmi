package com.kasai.robotgame;

import com.kasai.robotgame.entities.*;
import com.kasai.robotgame.events.GridEvents;
import com.kasai.robotgame.events.MovementEvent;
import com.kasai.robotgame.tiles.*;
import com.kasai.robotgame.events.SystemEvents;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

import static com.kasai.robotgame.Util.percentage;

public class GameState implements GridEvents, SystemEvents
{
    private final GameGrid gameGrid = new GameGrid();
    private static GameState instance = null;

    private GridEvents gridListener;
    private SystemEvents systemListener;
    private int movesCounter;
    private GameState() {

    }

    public static GameState getInstance()
    {
        if(instance == null) {
            instance = new GameState();
        }

        return instance;
    }

    /**
     * Initialize the game state with all the relative dependencies.
     *
     * @param side hold how long the map should be.
     * @param gridListener holds the listener for grid events.
     * @param systemListener holds the listener for system events.
     */
    public void init(int side, GridEvents gridListener, SystemEvents systemListener) {
        this.gridListener = gridListener;
        this.systemListener = systemListener;
        gameGrid.init(side, this);
        this.movesCounter = 0;
    }

    @Override
    public void onTileChange(Entity.Coordinates c, Entity e) {
        gridListener.onTileChange(c, e);
    }

    @Override
    public void onTileSwap(Entity.Coordinates a, Entity.Coordinates b) {
        gridListener.onTileSwap(a, b);
    }

    @Override
    public void showMessage(String message, int type) {
        systemListener.showMessage(message, type);
    }

    @Override
    public int showConfirmationDialog(String message, int type) {
        return systemListener.showConfirmationDialog(message, type);
    }

    @Override
    public Optional<File> saveFileDialog(String title) {
        return systemListener.saveFileDialog(title);
    }

    @Override
    public Optional<File> openFileDialog(String title) {
        return systemListener.openFileDialog(title);
    }

    /**
     * Handles all the operations to perform after a player's movement.
     */
    public void gameLoop()
    {
        this.handleHydraulics();
        this.handlePets();
    }

    private void handleHydraulics()
    {
        this.getGameGrid().getEntities(Hydraulic.class).forEach(hydraulic ->
        {
            var shouldBreak = Util.randomizer.nextBoolean();

            if(!hydraulic.getIsLeaking().get() && shouldBreak) {
                hydraulic.setLeaking(true);
            }
        });

        this.handleLeaks();
    }

    private void handleLeaks()
    {
        this.getGameGrid().getEntities(Hydraulic.class).forEach(hydraulic -> {
            if(hydraulic.getIsLeaking().get())
            {
                if(GameState.getInstance().getMovesCounter() % hydraulic.getFlowRate() == 0)
                    hydraulic.leaks(hydraulic);
            }
        });
    }
    private void handlePets()
    {
        this.getGameGrid().getEntities(Pet.class)
                .stream()
                .map(Pet::move)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(this.gameGrid::swapTiles);
    }

    public GameGrid getGameGrid() {
        return gameGrid;
    }

    /**
     * Contains all the logic related to the game's grid/map.
     */
    public class GameGrid implements MapInitializer
    {
        public final AtomicBoolean lockingGrid = new AtomicBoolean(false);

        private GridEvents listener;
        private Entity[][] grid;

        private int rows;
        private int columns;

        private GameGrid() {}

        /**
         * Initialize the map with all the entities available based on their spawn percentage values.
         *
         * @param side holds the map's side length.
         * @param listener holds the listener to grid events.
         */
        public void init(int side, GridEvents listener)
        {
            this.rows = side;
            this.columns = side;
            this.listener = listener;

            this.grid = new Entity[rows][columns];

            this.initWalls();

            try {
                this.initEntities();
            } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }

        private Stream<Entity> entities()
        {
            return Arrays.stream(this.grid)
                .map(Arrays::asList)
                .flatMap(Collection::stream);
        }

        public void enterAdminMode()
        {
            var choice = GameState.this.showConfirmationDialog("Sicuro? Non si puo' tornare alla visualizzazione normale", JOptionPane.INFORMATION_MESSAGE);

            if(choice == 1) return;
            gameGrid.entities().forEach(el -> el.setVisibility(true));
        }

        /**
         * Gives access to all the entities present onto the map.
         *
         * @return list of all the entities spawned.
         */
        public List<Entity> getEntities() {
            return this.entities().toList();
        }

        /**
         * Gives access to all the entities present onto the map filtered by a given sub-entity class.
         *
         * @param entityType holds the class to filter by.
         * @param <T> holds the type of the desired results, must be a {@link com.kasai.robotgame.entities.Entity Entity} child.
         * @return a list containing all the available entites filtered by the requested class/type
         */
        public <T extends Entity> List<T> getEntities(Class<T> entityType) {
            return this.entities()
                    .filter(entityType::isInstance)
                    .map(entityType::cast)
                    .toList();
        }

        /**
         * Safely edits a grid's tile.
         *
         * @param entity holds the entity to update.
         */
        public void editTile(Entity entity)
        {
            if(this.grid == null) {
                throw new IllegalCallerException("Game grid has not been initialized!");
            }

            lockingGrid.set(true);

            var coordinates = entity.getCoordinates();
            this.grid[coordinates.getX()][coordinates.getY()] = entity;

            lockingGrid.set(false);

            if (listener != null) {
                listener.onTileChange(coordinates, entity);
            }
        }

        /**
         * Easily swaps two grid's tiles.
         *
         * @param movementEvent holds the necessary data to perform the swap.
         */
        public void swapTiles(MovementEvent movementEvent) {
            var oldCoordinates = movementEvent.getOldPosition();
            var newCoordinates = movementEvent.getNewPosition();

            this.swapTiles(oldCoordinates, newCoordinates);
        }

        /**
         * Gives the possibility to fetch the entity that exists at specific coordinates
         *
         * @param coordinates holds the coordinates from where we want to fetch the entity.
         * @return the entity present at the supplied coordinates.
         */
        public Entity getTileAt(Entity.Coordinates coordinates) {
            return this.grid[coordinates.getX()][coordinates.getY()];
        }

        @Override
        public void exportToFile()
        {
            var selectedFile = GameState.this.saveFileDialog("Scegli la cartella dove salvare");

            if(selectedFile.isEmpty()) {
                return;
            }

            var fileName = "salvataggio_" + System.currentTimeMillis();
            try(var fw = new FileWriter(selectedFile.get() + "/" + fileName + ".txt")) {
                fw.write(this.toString());
                GameState.this.showMessage("Salvataggio completato con successo", JOptionPane.INFORMATION_MESSAGE);
            } catch (IOException e) {
                GameState.this.showMessage("Non è stato possibile completare il salvataggio", JOptionPane.ERROR_MESSAGE);
            }
        }

        @Override
        public void loadFromFile()
        {
            var selectedFile = GameState.this.openFileDialog("Scegli il salvataggio da caricare");

            if(selectedFile.isEmpty()) {
                return;
            }

            List<String> fileLines = new ArrayList<>();

            try (var br = Files.newBufferedReader(Path.of(selectedFile.get().getPath()))) {
                fileLines = br.lines().toList();
            } catch (IOException e) {
                GameState.this.showMessage("Non è stato possibile leggere il file selezionato", JOptionPane.ERROR_MESSAGE);
            }

            if(fileLines.isEmpty()) {
                GameState.this.showMessage("Il file selezionato è vuoto", JOptionPane.ERROR_MESSAGE);
                return;
            }

            var sideSize = fileLines.size();

            this.grid = new Entity[sideSize][sideSize];

            for(int x = 0; x < fileLines.size(); x++)
            {
                var charArray = fileLines.get(x).toCharArray();

                var col = 0;

                for(int y = 0; y < charArray.length;)
                {
                    var c = charArray[y];
                    var isVisible = c != '(' || charArray[y + 2] != ')';

                    if(!isVisible)
                    {
                        if(charArray[y + 2] != ')') {
                            GameState.this.showMessage("Quando si apre una parentesi per un determinato carattere dev'essere anche chiusa", JOptionPane.ERROR_MESSAGE);
                            this.reset();
                            return;
                        }

                        c = charArray[y+1];
                    }

                    try {
                        var entityClass = Entity.fromRepresentingChar(c);
                        var constructors = entityClass.getConstructors();

                        if (constructors.length == 0) {
                            var errorMessage = entityClass.getCanonicalName() + " has no available constructors";
                            throw new NoSuchMethodError(errorMessage);
                        }

                        Entity entity;
                        var coordinates = new Entity.Coordinates(x, col);

                        if(entityClass == Blank.class) {
                            var constructor = Arrays
                                .stream(entityClass.getConstructors())
                                .filter(el -> {
                                    if(el.getParameterTypes().length != 2) {
                                        return false;
                                    }

                                    return el.getParameterTypes()[0] == Entity.Coordinates.class &&
                                            el.getParameterTypes()[1] == Boolean.class;
                                })
                                .findAny();

                            if (constructor.isEmpty()) {
                                var errorMessage = entityClass.getCanonicalName() + " has no available constructors";
                                throw new NoSuchMethodError(errorMessage);
                            }

                            var isWet = c == '~';
                            entity = (Blank) constructor.get().newInstance(coordinates, isWet);
                        }
                        else if (entityClass == Sink.class) {
                            var isLeaking = c == '!';

                            var constructor = Arrays
                                    .stream(entityClass.getConstructors())
                                    .filter(el -> {
                                        if(el.getParameterTypes().length != 2) {
                                            return false;
                                        }

                                        return el.getParameterTypes()[0] == Entity.Coordinates.class &&
                                                el.getParameterTypes()[1] == Boolean.class;
                                    })
                                    .findAny();

                            if (constructor.isEmpty()) {
                                var errorMessage = entityClass.getCanonicalName() + " has no available constructors";
                                throw new NoSuchMethodError(errorMessage);
                            }

                            entity = (Sink) constructor.get().newInstance(coordinates, isLeaking);
                        } else if (entityClass == WashingMachine.class) {
                            var isLeaking = c == '@';

                            var constructor = Arrays
                                    .stream(entityClass.getConstructors())
                                    .filter(el -> {
                                        if(el.getParameterTypes().length != 2) {
                                            return false;
                                        }

                                        return el.getParameterTypes()[0] == Entity.Coordinates.class &&
                                                el.getParameterTypes()[1] == Boolean.class;
                                    })
                                    .findAny();

                            if (constructor.isEmpty()) {
                                var errorMessage = entityClass.getCanonicalName() + " has no available constructors";
                                throw new NoSuchMethodError(errorMessage);
                            }

                            entity = (WashingMachine) constructor.get().newInstance(coordinates, isLeaking);
                        } else if (entityClass == Stove.class) {
                            var status = c == 'x' ? Status.ON : Status.OFF;

                            var constructor = Arrays
                                    .stream(entityClass.getConstructors())
                                    .filter(el -> {
                                        if(el.getParameterTypes().length != 2) {
                                            return false;
                                        }

                                        return el.getParameterTypes()[0] == Entity.Coordinates.class &&
                                                el.getParameterTypes()[1] == Status.class;
                                    })
                                    .findAny();

                            if (constructor.isEmpty()) {
                                var errorMessage = entityClass.getCanonicalName() + " has no available constructors";
                                throw new NoSuchMethodError(errorMessage);
                            }

                            entity = (Stove) constructor.get().newInstance(coordinates, status);
                        } else {
                            var constructor = Arrays
                                    .stream(entityClass.getConstructors())
                                    .filter(el -> {
                                        if(el.getParameterTypes().length != 1) {
                                            return false;
                                        }

                                        return el.getParameterTypes()[0] == Entity.Coordinates.class;
                                    })
                                    .findAny();

                            if (constructor.isEmpty()) {
                                var errorMessage = entityClass.getCanonicalName() + " has no available constructors";
                                throw new NoSuchMethodError(errorMessage);
                            }

                            entity = (Entity) constructor.get().newInstance(coordinates);
                        }

                        entity.setVisibility(isVisible);

                        this.editTile(entity);
                    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                        GameState.this.showMessage("Errore inaspettato", JOptionPane.ERROR_MESSAGE);
                        this.reset();
                        return;
                    } catch (IllegalArgumentException e) {
                        GameState.this.showMessage(String.format("Il carattere %s non è valido", c), JOptionPane.ERROR_MESSAGE);
                        this.reset();
                        return;
                    }

                    if(!isVisible) {
                        y += 3;
                    } else {
                        y++;
                    }

                    col++;
                }
            }
        }

        @Override
        public void spawnPlayer()
        {
            var spawnCoordinates = new Entity.Coordinates(
                Util.randomizer.nextInt(1, rows - 1),
                Util.randomizer.nextInt(1, columns - 1)
            );

            var player = new Robot(spawnCoordinates);
            player.setPrevTileState(gameGrid.getTileAt(spawnCoordinates));
            this.editTile(player);
        }

        @Override
        public void initEntities() throws InvocationTargetException, InstantiationException, IllegalAccessException {
            var blanks = rows * columns;

            this.spawnPlayer();

            this.addMultipleEntitiesRandomly(percentage(blanks, Pet.SPAWN_PERCENTAGE), List.of(Cat.class, Dog.class));
            this.addMultipleEntitiesRandomly(percentage(blanks, Stove.SPAWN_PERCENTAGE), List.of(Stove.class));
            this.addMultipleEntitiesRandomly(percentage(blanks, Sink.SPAWN_PERCENTAGE), List.of(Sink.class));
            this.addMultipleEntitiesRandomly(percentage(blanks, WashingMachine.SPAWN_PERCENTAGE), List.of(WashingMachine.class));
        }

        @Override
        public void initWalls()
        {
            for(int i = 0; i < this.columns; i++)
            {
                for(int j = 0; j < this.rows; j++)
                {
                    var coordinates = new Entity.Coordinates(i, j);
                    Entity entity;

                    if(i == 0 || i == this.columns-1 || j == 0 || j == this.rows-1) {
                        entity = new Brick(coordinates);
                    }
                    else {
                        entity = new Blank(coordinates);
                    }

                    this.editTile(entity);
                }
            }
        }

        @Override
        public void reset() {
            GameState.this.setMovesCounter(0);

            this.grid = new Entity[rows][columns];

            this.initWalls();

            try {
                this.initEntities();
            } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                System.exit(-1);
            }
        }

        private void addMultipleEntitiesRandomly(int entitiesToGenerate, List<Class<? extends Entity>> classes)
                throws InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchMethodError {

            for(int i = 1; i <= entitiesToGenerate; i++)
            {
                var clazz = Util.randomElementFromList(classes);

                Entity.Coordinates coordinates;

                do
                {
                    coordinates = new Entity.Coordinates(
                        Util.randomizer.nextInt(1, rows - 1),
                        Util.randomizer.nextInt(1, columns - 1)
                    );

                } while(!(getTileAt(coordinates) instanceof Blank));

                var constructor = Arrays
                        .stream(clazz.getConstructors())
                        .filter(el -> {
                            if(el.getParameterTypes().length != 1) {
                                return false;
                            }

                            return el.getParameterTypes()[0] == Entity.Coordinates.class;
                        })
                        .findAny();

                if (constructor.isEmpty()) {
                    var errorMessage = clazz.getCanonicalName() + " has no available constructors";
                    throw new NoSuchMethodError(errorMessage);
                }

                var entity = (Entity) constructor.get().newInstance(coordinates);

                this.editTile(entity);
            }
        }

        private void swapTiles(Entity.Coordinates a, Entity.Coordinates b)
        {
            if(this.grid == null) {
                throw new IllegalCallerException("Game grid has not been initialized!");
            }

            lockingGrid.set(true);

            var support = this.grid[a.getX()][a.getY()];
            this.grid[a.getX()][a.getY()] = this.grid[b.getX()][b.getY()];
            this.grid[b.getX()][b.getY()] = support;

            lockingGrid.set(false);

            if (listener != null) {
                listener.onTileSwap(a, b);
            }
        }

        @Override
        public String toString()
        {
            var result = new StringBuilder();

            for(int i = 0; i < this.columns; i++)
            {
                for(int j = 0; j < this.rows; j++)
                {
                    var currEntity = this.grid[i][j];
                    result.append(currEntity.representingChar());
                }

                result.append("\n");
            }

            return result.toString();
        }
    }

    public int getMovesCounter() {
        return movesCounter;
    }

    public void setMovesCounter(int movesCounter) {
        this.movesCounter = movesCounter;
    }
}
