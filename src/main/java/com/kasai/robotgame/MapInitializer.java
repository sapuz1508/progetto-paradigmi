package com.kasai.robotgame;

import java.lang.reflect.InvocationTargetException;

public interface MapInitializer
{
    /**
     * Holds the logic to spawn the player correctly into the map.
     */
    void spawnPlayer();

    /**
     * Build the map's perimeter out of {@link com.kasai.robotgame.tiles.Brick Brick}.
     */
    void initWalls();

    /**
     * Allows the map to be exported to a human-readable and editable file.
     */
    void exportToFile();

    /**
     * Loads a previously exported file back into the game. Data integrity must be checked.
     */
    void loadFromFile();

    /**
     * Resets to a random state the entire map.
     */
    void reset();

    /**
     * Inits all the game entities into the map.
     *
     * @throws InvocationTargetException – if an underlying entity's constructor throws an exception.
     * @throws InstantiationException – if the entity that declares the underlying constructor represents an abstract class.
     * @throws IllegalAccessException – if the Constructor object is enforcing Java language access control and the underlying constructor is inaccessible.
     */
    void initEntities() throws InvocationTargetException, InstantiationException, IllegalAccessException;
}
