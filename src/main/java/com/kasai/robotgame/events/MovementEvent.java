package com.kasai.robotgame.events;

import com.kasai.robotgame.entities.Entity;

public class MovementEvent
{
    private Entity.Coordinates oldPosition;
    private Entity.Coordinates newPosition;

    public MovementEvent(Entity.Coordinates oldPosition, Entity.Coordinates newPosition) {
        this.oldPosition = oldPosition;
        this.newPosition = newPosition;
    }

    public Entity.Coordinates getOldPosition() {
        return oldPosition;
    }

    public void setOldPosition(Entity.Coordinates oldPosition) {
        this.oldPosition = oldPosition;
    }

    public Entity.Coordinates getNewPosition() {
        return newPosition;
    }

    public void setNewPosition(Entity.Coordinates newPosition) {
        this.newPosition = newPosition;
    }
}
