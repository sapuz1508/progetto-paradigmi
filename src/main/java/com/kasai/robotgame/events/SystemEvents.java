package com.kasai.robotgame.events;

import java.io.File;
import java.util.Optional;

public interface SystemEvents
{
    /**
     * Displays a dialog to the user.
     *
     * @param message holds the dialog's body.
     * @param messageType determines the dialog's type.
     */
    void showMessage(String message, int messageType);

    /**
     * Displays a dialog to the user asking for a confirmation.
     *
     * @param message holds the dialog's body.
     * @param messageType determines the dialog's type.
     * @return 0 if the user selects YES, 1 otherwise
     */
    int showConfirmationDialog(String message, int messageType);

    /**
     * Present the user the possibility to choose a directory where to save a file through a native form.
     *
     * @param title holds the form's title.
     * @return an eventual directory (as a File) if the user chose one.
     */
    Optional<File> saveFileDialog(String title);

    /**
     * Present the user the possibility to choose a file to be open through a native form.
     *
     * @param title holds the form's title
     * @return and eventual file if the user chose one.
     */
    Optional<File> openFileDialog(String title);
}
