package com.kasai.robotgame.events;

import com.kasai.robotgame.entities.Entity;

public interface GridEvents
{
    /**
     * This method is used and called upon when a tile/entity has been edited.
     *
     * @param coordinates holds the changed tile coordinates.
     * @param e holds the updated entity that has changed.
     */
    void onTileChange(Entity.Coordinates coordinates, Entity e);

    /**
     * This method is used and called upon when two tiles are to be swapped.
     *
     * EG: during movement.
     *
     * @param a holds the coordinates from where to start the swap.
     * @param b holds the coordinates to where end the swap.
     */
    void onTileSwap(Entity.Coordinates a, Entity.Coordinates b);
}
