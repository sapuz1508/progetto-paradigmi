package com.kasai.robotgame;

import java.util.List;
import java.util.Random;

public class Util
{
    public static final Random randomizer = new Random();

    private Util() {

    }

    /**
     * Calculates the percentage given an int and the percentage's value as float
     *
     * @param whole holds the number from which calculate the percentage
     * @param p holds the percentage's value expressed as a float (EG: 0.5 => 50%)
     * @return the calculated percentage
     */
    public static int percentage(int whole, float p) {
        return Math.round(whole * p);
    }

    /**
     * Filters a given list by matching a specific class within the list.
     *
     * @param list holds the list from where the data need to be taken.
     * @param entityType holds the class to filter by.
     * @param <T> holds the desired type to be taken out of the list.
     * @return a sublist from the original with all the elements already casted to the desired one.
     */
    public static <T> List<T> filterListByType(List<?> list, Class<T> entityType) {
        return list.stream()
                .filter(entityType::isInstance)
                .map(entityType::cast)
                .toList();
    }

    /**
     * Extract exactly one element out of a list randomly.
     *
     * @param elements list to be used a data set.
     * @param <T> list's content type.
     * @return a random element of the supplied list.
     */
    public static <T> T randomElementFromList(List<T> elements)
    {
        if(elements.size() == 1) {
            return elements.get(0);
        }

        var randomIndex = randomizer.nextInt(elements.size());
        return elements.get(randomIndex);
    }
}
