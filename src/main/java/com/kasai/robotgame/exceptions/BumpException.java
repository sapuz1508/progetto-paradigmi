package com.kasai.robotgame.exceptions;

public class BumpException extends Exception {

    public BumpException(String message) {
        super(message);
    }
}
