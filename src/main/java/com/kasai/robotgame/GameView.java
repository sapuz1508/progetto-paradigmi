package com.kasai.robotgame;

import com.kasai.robotgame.entities.Entity;
import com.kasai.robotgame.entities.Pet;
import com.kasai.robotgame.tiles.Blank;
import com.kasai.robotgame.events.GridEvents;
import com.kasai.robotgame.events.SystemEvents;
import com.kasai.robotgame.tiles.Robot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Optional;

public class GameView extends JFrame implements GridEvents, ActionListener, SystemEvents
{
    private final GameState gameState = GameState.getInstance();
    private final int side;

    private final JPanel panel = new JPanel();

    public GameView(int l)
    {
        super("Mondo Robot");

        this.side = l;

        this.setSize(l*75, l*75);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        panel.setLayout(new GridLayout(0, l, 2, -5));
        this.add(panel);

        setupMenu();

        gameState.init(l, this, this);

        this.setVisible(true);
    }

    private void setupMenu()
    {
        var menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);

        var menu = new JMenu("Options");
        menu.setMnemonic(KeyEvent.VK_O);
        menuBar.add(menu);

        var newGameMenuItem = new JMenuItem("New game", KeyEvent.VK_N);
        var loadGameMenuItem = new JMenuItem("Load game", KeyEvent.VK_L);
        var saveGameMenuItem = new JMenuItem("Save game", KeyEvent.VK_S);
        var viewAdminMenuItem = new JMenuItem("View admin", KeyEvent.VK_V);


        menu.add(newGameMenuItem);
        menu.add(saveGameMenuItem);
        menu.add(loadGameMenuItem);
        menu.add(viewAdminMenuItem);

        newGameMenuItem.addActionListener(this);
        loadGameMenuItem.addActionListener(this);
        saveGameMenuItem.addActionListener(this);
        viewAdminMenuItem.addActionListener(this);
    }

    @Override
    public Optional<File> saveFileDialog(String title)
    {
        var chooser = new JFileChooser();

        chooser.setDialogTitle(title);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            return Optional.of(chooser.getSelectedFile());
        }

        return Optional.empty();
    }

    @Override
    public Optional<File> openFileDialog(String title)
    {
        var chooser = new JFileChooser();

        chooser.setDialogTitle(title);
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            return Optional.of(chooser.getSelectedFile());
        }

        return Optional.empty();
    }

    @Override
    public void showMessage(String message, int type) {
        JOptionPane.showMessageDialog(this, message, "", type);
    }

    @Override
    public int showConfirmationDialog(String message, int type) {
        return JOptionPane.showConfirmDialog(this, message, "", JOptionPane.YES_NO_OPTION, type);
    }

    @Override
    public void onTileChange(Entity.Coordinates coordinates, Entity e)
    {
        var index = this.side * coordinates.getX() + coordinates.getY();
        var entity = gameState.getGameGrid().getTileAt(coordinates);

        try {
            panel.remove(index);
        }
        catch(IndexOutOfBoundsException ignored) {
            // No need to implement this catch.
        }

        panel.add(e, index);

        e.setVisibility(entity.getVisibility());

        panel.repaint();
        panel.revalidate();
    }

    @Override
    public void onTileSwap(Entity.Coordinates a, Entity.Coordinates b) {
        var indexA = this.side * a.getX() + a.getY();
        var indexB = this.side * b.getX() + b.getY();

        var tileA = panel.getComponent(indexA);
        var entityB = gameState.getGameGrid().getTileAt(b);

        var condition = (entityB instanceof Pet && entityB.getVisibility());

        panel.remove(indexA);
        panel.add(new Blank(a, false,  entityB instanceof Robot || condition), indexA);

        panel.remove(indexB);
        panel.add(tileA, indexB);

        panel.repaint();
        panel.revalidate();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case "New game" -> gameState.getGameGrid().reset();
            case "Load game" -> gameState.getGameGrid().loadFromFile();
            case "Save game" -> gameState.getGameGrid().exportToFile();
            case "View admin" -> gameState.getGameGrid().enterAdminMode();
        }
    }
}
