package com.kasai.robotgame.entities;

import com.kasai.robotgame.GameState;
import com.kasai.robotgame.Util;
import com.kasai.robotgame.events.MovementEvent;
import com.kasai.robotgame.tiles.Blank;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class Pet extends Entity
{
    /**
     * Represents the percentage this entity can spawn.
     */
    public static final float SPAWN_PERCENTAGE = 0.04f;

    protected Pet(Coordinates coordinates, Sprite sprite) {
        super(coordinates, sprite, false);
    }

    /**
     * Decides if the pet should move and eventually perform the action.
     *
     * @return an eventual movement event in the case the Pet has decided to move and if it can actually perform the movement.
     */
    public Optional<MovementEvent> move()
    {
        var shouldMove = Util.randomizer.nextBoolean();

        if(shouldMove)
        {
            List<Coordinates> availableCoordinates = new ArrayList<>();
            var gameGrid = GameState.getInstance().getGameGrid();

            for(var adjacent : this.adjacentCoordinates().values())
            {
                var entity = gameGrid.getTileAt(adjacent);

                if(entity.getCrossable())
                {
                    if(entity instanceof Blank blank && !blank.isWet()) {
                        availableCoordinates.add(adjacent);
                    }
                    else if(!(entity instanceof Blank)) {
                        availableCoordinates.add(adjacent);
                    }
                }
            }

            if(!availableCoordinates.isEmpty())
            {
                var index = Util.randomizer.nextInt(0, availableCoordinates.size());

                var randomCoordinates = availableCoordinates.get(index);
                var movement = new MovementEvent(getCoordinates(), randomCoordinates);

                this.setCoordinates(movement.getNewPosition());

                return Optional.of(movement);
            }
        }

        return Optional.empty();
    }
}
