package com.kasai.robotgame.entities;

import com.kasai.robotgame.GameState;
import com.kasai.robotgame.Util;
import com.kasai.robotgame.tiles.Blank;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public abstract class Hydraulic extends Entity
{
    private final transient GameState gameState = GameState.getInstance();

    private final AtomicBoolean isLeaking = new AtomicBoolean(false);

    //How much time between leaks (seconds)
    private final int flowRate;

    protected Hydraulic(Coordinates coordinates, Sprite sprite, int flowRate) {
        super(coordinates, sprite, false);
        this.flowRate = flowRate;
    }

    protected Hydraulic(Coordinates coordinates, Sprite sprite, int flowRate, boolean isLeaking) {
        super(coordinates, sprite, false);
        this.flowRate = flowRate;
        this.isLeaking.set(isLeaking);
    }

    private void stopLeakWorker() {
        this.isLeaking.set(false);
    }

    private void startLeakWorker() {
        this.isLeaking.set(true);
    }

    public AtomicBoolean getIsLeaking() {
        return isLeaking;
    }

    public void leaks(Entity entity)
    {

        var randomBlank = entity.randomBlankAdjacentCoordinates();
        if (!randomBlank.isEmpty())
            if (gameState.getGameGrid().getTileAt(randomBlank.get()) instanceof Blank blank)
            {
                if(blank.isWet())
                    this.leaks(blank);
                else
                    blank.setWet(true);
            }
    }



    public int getFlowRate() {
        return flowRate;
    }

    public void setLeaking(boolean leaking)
    {
        if(leaking && !isLeaking.get()) {
            this.startLeakWorker();
        }
        else if(!leaking && isLeaking.get()) {
            this.stopLeakWorker();
        }
    }

    @Override
    public void interact() {
        this.setLeaking(false);
    }
}
