package com.kasai.robotgame.entities;

public enum ActionKey {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    SPACEBAR,
    ENTER;

    /**
     *
     * @return if the current instance is an action.
     */
    public boolean isAction() {
        return this == ActionKey.SPACEBAR || this == ActionKey.ENTER || this == ActionKey.DOWN;
    }

    /**
     *
     * @return if the current instance is a movement.
     */
    public boolean isMovement() { return this == ActionKey.UP; }
}
