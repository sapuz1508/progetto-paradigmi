package com.kasai.robotgame.entities;

import com.kasai.robotgame.GameState;
import com.kasai.robotgame.Util;
import com.kasai.robotgame.tiles.*;
import com.kasai.robotgame.tiles.Robot;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Entity extends JPanel implements Interactable
{
    private final UUID id = UUID.randomUUID();

    protected Coordinates coordinates;
    private final Sprite sprite;
    private Sprite actualSprite;
    private boolean crossable;

    private boolean isVisible;

    private final JLabel image;

    protected Entity(Coordinates coordinates, Sprite sprite, boolean crossable) {
        this.coordinates = coordinates;
        this.sprite = sprite;
        this.crossable = crossable;
        this.actualSprite = sprite;

        var icon = new ImageIcon(sprite.getImage());
        this.image = new JLabel(icon);

        this.setVisibility(false);

        this.image.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        this.add(this.image);
    }

    /**
     * Calculates the adjacent coordinates based onto the current ones.
     *
     * @return the adjacent mapping's coordinates by orientation.
     */
    public Map<Orientation, Coordinates> adjacentCoordinates()
    {
        var x = this.coordinates.getX();
        var y = this.coordinates.getY();

        return Map.of(
            Orientation.WEST, new Coordinates(x, y - 1),
            Orientation.EAST, new Coordinates(x, y + 1),
            Orientation.NORTH, new Coordinates(x - 1, y),
            Orientation.SOUTH, new Coordinates(x + 1, y)
        );
    }

    /**
     *
     * Simplify the access to the adjacent entities. It is based on {@link #adjacentCoordinates() adjacentCoordinates}.
     * @return the adjacent mapping's entities by orientation.
     */
    public Map<Orientation, Entity> adjacentEntities()
    {
        var gameGrid = GameState.getInstance().getGameGrid();

        Map<Orientation, Entity> adjacentEntities = new HashMap<>();
        final var coordinates = adjacentCoordinates();

        adjacentEntities.put(Orientation.NORTH, gameGrid.getTileAt(coordinates.get(Orientation.NORTH)));
        adjacentEntities.put(Orientation.SOUTH, gameGrid.getTileAt(coordinates.get(Orientation.SOUTH)));
        adjacentEntities.put(Orientation.WEST, gameGrid.getTileAt(coordinates.get(Orientation.WEST)));
        adjacentEntities.put(Orientation.EAST, gameGrid.getTileAt(coordinates.get(Orientation.EAST)));

        return adjacentEntities;
    }

    /**
     * Represents the adjacent tiles as a list. It is based on {@link #adjacentEntities() adjacentEntities}.
     *
     * @return the adjacent entities as list.
     */
    public List<Entity> adjacentEntitiesList() {
        return this.adjacentEntities().values().stream().toList();
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public Entity setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
        return this;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public Entity setSprite(Sprite sprite) {
        this.actualSprite = sprite;

        var icon = new ImageIcon(actualSprite.getImage());
        this.image.setIcon(icon);
        return this;
    }

    public boolean getCrossable() {
        return crossable;
    }

    public Entity setCrossable(boolean crossable) {
        this.crossable = crossable;
        return this;
    }

    /**
     * This method must be implemented in order to give each possible entity its unique string value.
     *
     * @return the char that represents the entity.
     */
    public abstract String representingChar();

    public static Class<? extends Entity> fromRepresentingChar(char c)
    {
        return switch (c) {
            case 'P' -> Robot.class;
            case '~', '-' -> Blank.class;
            case '#' -> Brick.class;
            case 'D' -> Dog.class;
            case 'C' -> Cat.class;
            case '!', '|' -> Sink.class;
            case 'x', '*' -> Stove.class;
            case '@', 'O' -> WashingMachine.class;
            default -> throw new IllegalArgumentException("No entity uses: " + c);
        };
    }

    public UUID getId() {
        return id;
    }

    public boolean getVisibility() {
        return isVisible;
    }

    public void setVisibility(boolean visible) {
        isVisible = visible;
        setSprite(isVisible ? this.sprite : Sprite.FOG);
    }

    public static class Coordinates
    {
        private int x;
        private int y;

        public Coordinates(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public static Coordinates random(int min, int max)
        {
            var x = Util.randomizer.nextInt(max - min + 1) + min;
            var y = Util.randomizer.nextInt(max - min + 1) + min;

            return new Coordinates(x, y);
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        @Override
        public String toString() {
            return "Coordinates{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

    public Optional<Coordinates> randomBlankAdjacentCoordinates()
    {
        var a = this.adjacentEntitiesList();
        var nonWetTiles = Util.filterListByType(a, Blank.class)
                .stream()
                .collect(Collectors.toList());


        if(nonWetTiles.size() <= 0)
            return Optional.empty();

        var randomIndex = Util.randomizer.nextInt(nonWetTiles.size());

        var coordinates = new Coordinates(
                nonWetTiles.get(randomIndex).getCoordinates().getX(),
                nonWetTiles.get(randomIndex).getCoordinates().getY()
        );

        return Optional.of(coordinates);
    }
}
