package com.kasai.robotgame.entities;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public enum Sprite
{
    BLANK("blank.jpg"),
    BRICKS("bricks.jpg"),
    ROBOT_UP("robot_up.png"),
    ROBOT_DOWN("robot_down.png"),
    ROBOT_LEFT("robot_left.png"),
    ROBOT_RIGHT("robot_right.png"),
    STOVE_ON("stove_on.png"),
    STOVE_OFF("stove_off.png"),
    DOG("dog.png"),
    CAT("cat.png"),
    SINK("water_tap.png"),
    SPLASH("splash.png"),
    FOG("fog.png"),
    WASHING_MACHINE("washing machine.png");

    private BufferedImage image;

    Sprite(String fileName) {
        try {
            var classloader = Thread.currentThread().getContextClassLoader();
            var file = classloader.getResourceAsStream(fileName);

            this.image = ImageIO.read(file);
        } catch (IOException e) {
            // No need to implement this.
        }
    }

    public BufferedImage getImage() {
        return image;
    }
}
