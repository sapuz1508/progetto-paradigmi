package com.kasai.robotgame.entities;

public enum Orientation {
    NORTH, EAST, SOUTH, WEST
}
