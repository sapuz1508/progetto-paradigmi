package com.kasai.robotgame.entities;

public interface Interactable
{
    /**
     * Defines the entity's interaction with the player.
     */
    default void interact() {
        throw new IllegalCallerException("No interaction available!");
    }
}
