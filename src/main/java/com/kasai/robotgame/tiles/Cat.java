package com.kasai.robotgame.tiles;

import com.kasai.robotgame.GameState;
import com.kasai.robotgame.entities.Sprite;
import com.kasai.robotgame.entities.Pet;

import javax.swing.*;

public class Cat extends Pet
{
    private final GameState gameState = GameState.getInstance();

    public Cat(Coordinates coordinates) {
        super(coordinates, Sprite.CAT);
    }

    @Override
    public void interact() {
        gameState.showMessage("Meow!", JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public String representingChar() {
        return this.getVisibility() ? "C" : "(C)";
    }
}
