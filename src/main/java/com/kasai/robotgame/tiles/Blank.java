package com.kasai.robotgame.tiles;

import com.kasai.robotgame.entities.Sprite;
import com.kasai.robotgame.entities.Entity;

public class Blank extends Entity
{
    private Boolean wet;

    public Blank(Coordinates coordinates) {
        super(coordinates, Sprite.BLANK, true);
        this.wet = false;
    }

    public Blank(Coordinates coordinates, Boolean wet) {
        super(coordinates, Sprite.BLANK, true);
        this.wet = wet;
    }

    public Blank(Coordinates coordinates, Boolean wet, boolean visible) {
        super(coordinates, Sprite.BLANK, true);
        this.wet = wet;
        this.setVisibility(visible);
    }

    public Boolean isWet() {
        return wet;
    }

    public void setWet(Boolean wet) {
        this.wet = wet;
        if(getVisibility())
        {
            this.setSprite(wet ? Sprite.SPLASH : Sprite.BLANK);
        }
    }

    @Override
    public String representingChar()
    {
        var character = this.isWet() ? "~" : "-";
        return this.getVisibility() ? character : "(" + character + ")";
    }

    @Override
    public void setVisibility(boolean visible) {
        super.setVisibility(visible);
        if(visible)
        {
            this.setSprite(isWet() ? Sprite.SPLASH : Sprite.BLANK);
        }
    }
}
