package com.kasai.robotgame.tiles;

import com.kasai.robotgame.GameState;
import com.kasai.robotgame.entities.Sprite;
import com.kasai.robotgame.entities.Pet;

import javax.swing.*;

public class Dog extends Pet
{
    private final GameState gameState = GameState.getInstance();

    public Dog(Coordinates coordinates) {
        super(coordinates, Sprite.DOG);
    }

    @Override
    public void interact() {
        gameState.showMessage("Woof Woof!", JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public String representingChar() {
        return this.getVisibility() ? "D" : "(D)";
    }
}
