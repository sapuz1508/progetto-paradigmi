package com.kasai.robotgame.tiles;

import com.kasai.robotgame.GameState;
import com.kasai.robotgame.entities.Sprite;
import com.kasai.robotgame.Util;
import com.kasai.robotgame.entities.Entity;

import javax.swing.*;

public class Stove extends Entity
{
    public static final float SPAWN_PERCENTAGE = 0.03f;

    private Status stoveStatus;
    private final GameState gameState = GameState.getInstance();

    public Stove(Coordinates coordinates) {
        super(coordinates, Sprite.STOVE_OFF, false);
        this.setStoveStatus(Status.values()[Util.randomizer.nextInt(0, 2)]);
    }

    public Stove(Coordinates coordinates, Status stoveStatus) {
        super(coordinates, Sprite.STOVE_OFF, false);
        this.setStoveStatus(stoveStatus);
    }

    @Override
    public void interact()
    {
        if(this.stoveStatus == Status.ON) {
            this.setStoveStatus(Status.OFF);
        }
        else {
            gameState.showMessage("The stove is already OFF", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public Status getStoveStatus() { return stoveStatus; }

    public void setStoveStatus(Status stoveStatus) {
        this.stoveStatus = stoveStatus;
        this.setSprite(this.stoveStatus == Status.ON ? Sprite.STOVE_ON : Sprite.STOVE_OFF);
    }

    @Override
    public String representingChar() {
        var character = this.stoveStatus == Status.ON ? "x" : "*";
        return this.getVisibility() ? character : "(" + character + ")";
    }
}
