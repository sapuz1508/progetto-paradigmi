package com.kasai.robotgame.tiles;

import com.kasai.robotgame.entities.Sprite;
import com.kasai.robotgame.entities.Hydraulic;

public class WashingMachine extends Hydraulic
{
    public static final float SPAWN_PERCENTAGE = 0.01f;
    public static final int DEFAULT_FLOW_RATE = 7;

    public WashingMachine(Coordinates coordinates) {
        super(coordinates, Sprite.WASHING_MACHINE, DEFAULT_FLOW_RATE);
    }

    public WashingMachine(Coordinates coordinates, int flowRate) {
        super(coordinates, Sprite.SINK, flowRate);
    }

    public WashingMachine(Coordinates coordinates, Boolean isLeaking) {
        super(coordinates, Sprite.WASHING_MACHINE, DEFAULT_FLOW_RATE, isLeaking);
    }

    @Override
    public String representingChar() {
        var character = this.getIsLeaking().get() ? "@" : "O";
        return this.getVisibility() ? character : "(" + character + ")";
    }
}
