package com.kasai.robotgame.tiles;

import com.kasai.robotgame.GameState;
import com.kasai.robotgame.entities.Sprite;
import com.kasai.robotgame.entities.ActionKey;
import com.kasai.robotgame.entities.Entity;
import com.kasai.robotgame.entities.Orientation;
import com.kasai.robotgame.events.MovementEvent;
import com.kasai.robotgame.exceptions.BumpException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Map;

public class Robot extends Entity
{
    private Entity prevTileState;

    private static final int IFW = JComponent.WHEN_IN_FOCUSED_WINDOW;
    private final Map<ActionKey, ActionHandler> actionMap = Map.of(
        ActionKey.UP, new ActionHandler(this, ActionKey.UP),
        ActionKey.RIGHT, new ActionHandler(this, ActionKey.RIGHT),
        ActionKey.LEFT, new ActionHandler(this, ActionKey.LEFT),
        ActionKey.DOWN, new ActionHandler(this, ActionKey.DOWN),
        ActionKey.SPACEBAR, new ActionHandler(this, ActionKey.SPACEBAR),
        ActionKey.ENTER, new ActionHandler(this, ActionKey.ENTER)
    );

    private Orientation orientation;

    public Robot(Coordinates coordinates)
    {
        super(coordinates, Sprite.ROBOT_UP,false);

        this.orientation = Orientation.NORTH;

        setVisibility(true);

        this.getInputMap(IFW).put(KeyStroke.getKeyStroke("UP"), ActionKey.UP);
        this.getActionMap().put(ActionKey.UP, actionMap.get(ActionKey.UP));

        this.getInputMap(IFW).put(KeyStroke.getKeyStroke("RIGHT"), ActionKey.RIGHT);
        this.getActionMap().put(ActionKey.RIGHT, actionMap.get(ActionKey.RIGHT));

        this.getInputMap(IFW).put(KeyStroke.getKeyStroke("LEFT"), ActionKey.LEFT);
        this.getActionMap().put(ActionKey.LEFT, actionMap.get(ActionKey.LEFT));

        this.getInputMap(IFW).put(KeyStroke.getKeyStroke("DOWN"), ActionKey.DOWN);
        this.getActionMap().put(ActionKey.DOWN, actionMap.get(ActionKey.DOWN));

        this.getInputMap(IFW).put(KeyStroke.getKeyStroke("SPACE"), ActionKey.SPACEBAR);
        this.getActionMap().put(ActionKey.SPACEBAR, actionMap.get(ActionKey.SPACEBAR));

        this.getInputMap(IFW).put(KeyStroke.getKeyStroke("ENTER"), ActionKey.ENTER);
        this.getActionMap().put(ActionKey.ENTER, actionMap.get(ActionKey.ENTER));
    }

    /**
     * Changes entity's orientation in a clockwise path.
     */
    public void rotateRight()
    {
        final var directions = Orientation.values();
        var newIndex = this.orientation.ordinal() + 1;

        if(newIndex > directions.length - 1) newIndex = 0;

        this.orientation = directions[newIndex];
    }

    /**
     * Changes entity's orientation in an anti-clockwise path.
     */
    public void rotateLeft()
    {
        final var directions = Orientation.values();
        var newIndex = this.orientation.ordinal() - 1;

        if(newIndex < 0) newIndex = directions.length - 1;

        this.orientation = directions[newIndex];
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation orientation) {
        this.orientation = orientation;
    }

    @Override
    public void setVisibility(boolean visible) {
        super.setVisibility(visible);

        if(this.orientation == null) {
            return;
        }

        var newSprite = switch (this.orientation) {
            case NORTH -> Sprite.ROBOT_UP;
            case WEST -> Sprite.ROBOT_LEFT;
            case SOUTH -> Sprite.ROBOT_DOWN;
            case EAST -> Sprite.ROBOT_RIGHT;
        };

        setSprite(visible ? newSprite : Sprite.FOG);
    }

    @Override
    public String representingChar() {
        return this.getVisibility() ? "P" : "(P)";
    }

    private static class ActionHandler extends AbstractAction
    {
        private final transient GameState gameState = GameState.getInstance();

        private final Robot player;
        private final ActionKey keyPressed;

        public ActionHandler(Robot r, ActionKey keyPressed) {
            this.player = r;
            this.keyPressed = keyPressed;
        }

        private MovementEvent performMovement() throws BumpException
        {
            var currCoordinates = this.player.getCoordinates();
            var newCoordinates = switch (this.player.getOrientation()) {
                case NORTH -> new Entity.Coordinates(currCoordinates.getX() - 1, currCoordinates.getY());
                case SOUTH -> new Entity.Coordinates(currCoordinates.getX() + 1, currCoordinates.getY());
                case WEST -> new Entity.Coordinates(currCoordinates.getX(), currCoordinates.getY() - 1);
                case EAST -> new Entity.Coordinates(currCoordinates.getX(), currCoordinates.getY() + 1);
            };


            var futureTile = this.gameState.getGameGrid().getTileAt(newCoordinates);
            this.player.setPrevTileState(futureTile);

            if(!futureTile.getCrossable()) {
                throw new BumpException(
                        String.format("Tile (X: %s, Y: %s) is not crossable!",
                                futureTile.getCoordinates().getX(),
                                futureTile.getCoordinates().getY()
                        )
                );
            }
            else
                gameState.setMovesCounter(gameState.getMovesCounter() + 1);
            return new MovementEvent(currCoordinates, newCoordinates);
        }

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if(this.keyPressed.isAction())
            {
                var facingTile = this.player.adjacentCoordinates().get(this.player.getOrientation());
                var facingEntity = this.gameState.getGameGrid().getTileAt(facingTile);

                if(this.keyPressed.equals(ActionKey.ENTER))
                {
                    if(facingEntity instanceof Stove stove) {
                        this.gameState.showMessage("Stove Status: " + stove.getStoveStatus(), JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                else if(this.keyPressed.equals(ActionKey.SPACEBAR)) {
                    facingEntity.interact();
                }
                else if(this.keyPressed.equals(ActionKey.DOWN))
                {
                    if(this.player.getPrevTileState() instanceof Blank blank)
                    {
                        var message = blank.isWet() ? "This tile is wet!" : "This tile is not wet!";
                        this.gameState.showMessage(message, JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }
            else if(this.keyPressed.isMovement())
            {
                try
                {
                    var movementEvent = this.performMovement();

                    gameState.getGameGrid()
                            .getTileAt(movementEvent.getNewPosition())
                            .adjacentEntitiesList()
                            .forEach((entity) -> entity.setVisibility(true));

                    gameState.getGameGrid().swapTiles(movementEvent);
                    player.setCoordinates(movementEvent.getNewPosition());
                    gameState.gameLoop();
                } catch(BumpException bumpException) {
                    this.gameState.showMessage(bumpException.getMessage(), JOptionPane.ERROR_MESSAGE);
                }
            }
            else
            {
                if(this.keyPressed == ActionKey.RIGHT) {
                    this.player.rotateRight();
                }

                if(this.keyPressed == ActionKey.LEFT) {
                    this.player.rotateLeft();
                }

                var newSprite = switch (this.player.getOrientation()) {
                    case NORTH -> Sprite.ROBOT_UP;
                    case WEST -> Sprite.ROBOT_LEFT;
                    case SOUTH -> Sprite.ROBOT_DOWN;
                    case EAST -> Sprite.ROBOT_RIGHT;
                };

                this.player.setSprite(newSprite);
                this.player.repaint();
            }
        }
    }

    public Entity getPrevTileState() {
        return prevTileState;
    }

    public void setPrevTileState(Entity prevTileState) {
        this.prevTileState = prevTileState;
    }
}
