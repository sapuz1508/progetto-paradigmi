package com.kasai.robotgame.tiles;

import com.kasai.robotgame.entities.Sprite;
import com.kasai.robotgame.entities.Entity;

public class Brick extends Entity
{
    public Brick(Coordinates coordinates) {
        super(coordinates, Sprite.BRICKS, false);
        setVisibility(true);
    }

    @Override
    public String representingChar() {
        return this.getVisibility() ? "#" : "(#)";
    }
}
