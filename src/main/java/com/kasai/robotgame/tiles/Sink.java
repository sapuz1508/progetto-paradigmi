package com.kasai.robotgame.tiles;

import com.kasai.robotgame.entities.Sprite;
import com.kasai.robotgame.entities.Hydraulic;

public class Sink extends Hydraulic
{
    public static final float SPAWN_PERCENTAGE = 0.02f;
    public static final int DEFAULT_FLOW_RATE = 10;

    public Sink(Coordinates coordinates) {
        super(coordinates, Sprite.SINK, DEFAULT_FLOW_RATE);
    }

    public Sink(Coordinates coordinates, int flowRate) {
        super(coordinates, Sprite.SINK, flowRate);
    }

    public Sink(Coordinates coordinates, Boolean isLeaking) {
        super(coordinates, Sprite.SINK, DEFAULT_FLOW_RATE, isLeaking);
    }

    @Override
    public String representingChar() {
        var character = this.getIsLeaking().get() ? "!" : "|";
        return this.getVisibility() ? character : "(" + character + ")";
    }
}
