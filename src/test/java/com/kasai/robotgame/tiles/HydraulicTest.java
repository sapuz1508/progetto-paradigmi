package com.kasai.robotgame.tiles;

import com.kasai.robotgame.GameView;
import com.kasai.robotgame.entities.Entity;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;


import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class HydraulicTest {
    Sink sink;
    WashingMachine washingMachine;

    @BeforeAll
    void setUp() {
        GameView gameView = new GameView(11);
        sink = new Sink(new Entity.Coordinates(5,5));
        Sink secondSink = new Sink(new Entity.Coordinates(6,5),1);
        washingMachine = new WashingMachine(new Entity.Coordinates(4,5));
        WashingMachine secondWashingMachine = new WashingMachine(new Entity.Coordinates(4,6),1);
    }

    @Test
    void getFlowRate() {
        assertEquals(1, sink.getFlowRate());
    }

    @Test
    void leaking() {
        sink.setLeaking(true);
        AtomicBoolean atomicBoolean = sink.getIsLeaking();
        assertTrue(atomicBoolean.get());
        sink.setLeaking(false);
    }

    @Test
    void interact() {
        sink.interact();
        assertFalse(sink.getIsLeaking().get());
    }

}