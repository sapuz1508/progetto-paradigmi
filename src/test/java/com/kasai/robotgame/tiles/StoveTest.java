package com.kasai.robotgame.tiles;

import com.kasai.robotgame.entities.Entity;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StoveTest {
    Stove stove;

    @BeforeAll
    void setUp() {
        stove = new Stove(new Entity.Coordinates(5,5));
    }

    @Test
    void interact() {
        stove.setStoveStatus(Status.ON);
        stove.interact();

        assertEquals(Status.OFF,stove.getStoveStatus());

        stove.interact();
        assertEquals(Status.OFF,stove.getStoveStatus());

    }
}