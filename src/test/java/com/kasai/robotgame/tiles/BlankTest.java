package com.kasai.robotgame.tiles;

import com.kasai.robotgame.entities.Sprite;
import com.kasai.robotgame.entities.Entity;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BlankTest
{
    Blank blank;
    Blank notWetBlank;

    @BeforeAll
    void init() {
        blank = new Blank(new Entity.Coordinates(5,5));
        notWetBlank = new Blank(new Entity.Coordinates(5,5),false);
    }

    @Test
    void setWet() {
        assertFalse(notWetBlank.isWet());
        notWetBlank.setWet(true);
        assertTrue(notWetBlank.isWet());
    }

    @Test
    void interact() {
        assertThrows(IllegalCallerException.class, () -> blank.interact());
    }

    @Test
    void entityMethod() {
        assertEquals(Sprite.BLANK,blank.getSprite());
        blank.setCrossable(true);
        assertTrue(blank.getCrossable());
}
}