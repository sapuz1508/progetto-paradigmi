package com.kasai.robotgame.tiles;

import com.kasai.robotgame.entities.Entity;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertThrows;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BrickTest {
    Brick brick;

    @BeforeAll
    void setUp() {
        brick = new Brick(new Entity.Coordinates(5, 5));
    }

    @Test
    void interact() {
        assertThrows(IllegalCallerException.class, () -> brick.interact());
    }
}