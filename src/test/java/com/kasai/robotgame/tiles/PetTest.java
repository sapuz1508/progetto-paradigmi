package com.kasai.robotgame.tiles;

import com.kasai.robotgame.GameState;
import com.kasai.robotgame.GameView;
import com.kasai.robotgame.entities.Entity;
import com.kasai.robotgame.entities.Orientation;
import com.kasai.robotgame.entities.Pet;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PetTest {
    Pet pet;

    @BeforeAll
    void setUp() {
        GameView gameView = new GameView(11);

        pet = new Cat(new Entity.Coordinates(5, 5));
        UUID petID = pet.getId();
        assertNotNull(petID);
    }

    @Test
    void move() {
        for (int i = 0; i < 100; i++) {
            pet.move();
        }
        assertNotEquals(new Entity.Coordinates(5,5), pet.getCoordinates());
    }

    @Test
    void interact() {
        assertDoesNotThrow(() -> pet.interact());
    }

    @BeforeEach
    @Test
    void adjacentCoordinates(){
        var petX = pet.getCoordinates().getX();
        var petY = pet.getCoordinates().getY();
        var expectedCoordinates = new HashMap<Orientation, Entity.Coordinates>();
        expectedCoordinates.put(Orientation.NORTH, new Entity.Coordinates(petX-1, petY));
        expectedCoordinates.put(Orientation.SOUTH, new Entity.Coordinates(petX+1, petY));
        expectedCoordinates.put(Orientation.EAST, new Entity.Coordinates(petX, petY+1));
        expectedCoordinates.put(Orientation.WEST, new Entity.Coordinates(petX, petY-1));

        var actualCoordinates = pet.adjacentCoordinates();

        assertEquals(expectedCoordinates.get(Orientation.NORTH).getX(),actualCoordinates.get(Orientation.NORTH).getX());
        assertEquals(expectedCoordinates.get(Orientation.NORTH).getY(),actualCoordinates.get(Orientation.NORTH).getY());
        assertEquals(expectedCoordinates.get(Orientation.SOUTH).getX(),actualCoordinates.get(Orientation.SOUTH).getX());
        assertEquals(expectedCoordinates.get(Orientation.SOUTH).getY(),actualCoordinates.get(Orientation.SOUTH).getY());
        assertEquals(expectedCoordinates.get(Orientation.EAST).getX(),actualCoordinates.get(Orientation.EAST).getX());
        assertEquals(expectedCoordinates.get(Orientation.EAST).getY(),actualCoordinates.get(Orientation.EAST).getY());
        assertEquals(expectedCoordinates.get(Orientation.WEST).getX(),actualCoordinates.get(Orientation.WEST).getX());
        assertEquals(expectedCoordinates.get(Orientation.WEST).getY(),actualCoordinates.get(Orientation.WEST).getY());
    }

    @Test
    void adjacentEntities(){
        Map<Orientation,Entity> expectedEntities = new HashMap<>();

        var actualCoordinates = pet.adjacentCoordinates();
        var actualEntities = pet.adjacentEntities();
        expectedEntities.put(Orientation.NORTH,GameState.getInstance().getGameGrid().getTileAt(actualCoordinates.get(Orientation.NORTH)));
        expectedEntities.put(Orientation.SOUTH,GameState.getInstance().getGameGrid().getTileAt(actualCoordinates.get(Orientation.SOUTH)));
        expectedEntities.put(Orientation.EAST,GameState.getInstance().getGameGrid().getTileAt(actualCoordinates.get(Orientation.EAST)));
        expectedEntities.put(Orientation.WEST,GameState.getInstance().getGameGrid().getTileAt(actualCoordinates.get(Orientation.WEST)));

        assertEquals(expectedEntities.get(Orientation.NORTH).getClass(),actualEntities.get(Orientation.NORTH).getClass());
        assertEquals(expectedEntities.get(Orientation.SOUTH).getClass(),actualEntities.get(Orientation.SOUTH).getClass());
        assertEquals(expectedEntities.get(Orientation.EAST).getClass(),actualEntities.get(Orientation.EAST).getClass());
        assertEquals(expectedEntities.get(Orientation.WEST).getClass(),actualEntities.get(Orientation.WEST).getClass());

        var expectedEntitiesList = actualEntities.values().stream().toList();
        var actualEntitiesList = pet.adjacentEntitiesList();
        assertEquals(expectedEntitiesList, actualEntitiesList);
    }
}