package com.kasai.robotgame.entities;

import com.kasai.robotgame.tiles.Robot;
import com.kasai.robotgame.exceptions.BumpException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RobotTest {
    Robot robot;

    @BeforeAll
    void setUp() {
        robot = new Robot(new Entity.Coordinates(5,5));
    }

    @Test
    void rotateRight() {
        robot.setOrientation(Orientation.NORTH);
        assertEquals(Orientation.NORTH,robot.getOrientation());
        robot.rotateRight();
        assertEquals(Orientation.EAST,robot.getOrientation());
    }

    @Test
    void rotateLeft() {
        robot.setOrientation(Orientation.NORTH);
        robot.rotateLeft();
        assertEquals(Orientation.WEST,robot.getOrientation());

    }

    @Test
    void interact() {
        assertThrows(IllegalCallerException.class, () -> robot.interact());
    }

    @Test
    void throwBump() {
        try
        {
            throw new BumpException("Tile not Crossable");
        }catch(Exception e)
        {
            assertEquals("Tile not Crossable", e.getMessage());
        }
    }
}