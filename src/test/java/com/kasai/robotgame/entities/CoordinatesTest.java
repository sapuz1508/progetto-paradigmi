package com.kasai.robotgame.entities;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CoordinatesTest {
    Entity.Coordinates coordinates;
    Entity.Coordinates randomCoordinates;

    @BeforeAll
    void setUp() {
        coordinates = new Entity.Coordinates(5,5);
    }

    @Test
    void random() {
        for (int i = 0; i < 1000; i++) {
            randomCoordinates = Entity.Coordinates.random(5,10);

            assertTrue((randomCoordinates.getX() >= 5 && (randomCoordinates.getX() <= 10)));
            assertTrue((randomCoordinates.getY() >= 5 && (randomCoordinates.getY() <= 10)));
        }
    }

    @Test
    void testX() {
        assertEquals(5,coordinates.getX());
        coordinates.setX(6);
        assertEquals(6,coordinates.getX());
    }


    @Test
    void testY() {
        assertEquals(5,coordinates.getY());
        coordinates.setY(6);
        assertEquals(6,coordinates.getY());
    }

    @Test
    void testToString() {
        String s = "Coordinates{x=5, y=5}";
        assertEquals(s,coordinates.toString());
    }
}